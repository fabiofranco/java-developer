FROM ubuntu:16.04

ADD apache-maven-3.5.2-bin.tar.gz /opt/
ADD jdk-7u80-linux-x64.tar.gz /opt/
ADD jdk-8u162-linux-x64.tar.gz /opt/
ADD eclipse-jee-oxygen-2-linux-gtk-x86_64.tar.gz /opt/
RUN mv /opt/eclipse /opt/eclipse-jee
ADD eclipse-rcp-oxygen-2-linux-gtk-x86_64.tar.gz /opt/
RUN mv /opt/eclipse /opt/eclipse-rcp

RUN apt-get update -qq && \
    apt-get install unzip unrar locales tree vim htop git subversion build-essential sudo ssh libgtk-3-0 libxtst-dev python-setuptools python-dev groff build-essential -qq
	
RUN mkdir -p /home/developer && \
	export uid=1000 gid=1000 && \
	echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
	echo "developer:x:${uid}:" >> /etc/group && \
	echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
	chmod 0440 /etc/sudoers.d/developer && \
	\
	chown ${uid}:${gid} -R /home/developer && \
	chown ${uid}:${gid} -R /opt/* && \
	ln -s /opt/apache-maven-3.5.2/bin/mvn /bin/mvn && \
	ln -s /opt/jdk1.8.0_162/bin/java /bin/java && \
	ln -s /opt/eclipse-jee/eclipse /bin/eclipse-jee && \
	ln -s /opt/eclipse-rcp/eclipse /bin/eclipse-rcp && \
	locale-gen en_US.UTF-8 && \
	\
	apt-get autoclean -qq && \
	apt-get autoremove -qq && \
	rm -rf /var/lib/apt/lists/*

RUN easy_install pip &&	pip install awscli --upgrade

ENV HOME=/home/developer JAVA_HOME=/opt/jdk1.8.0_162 LC_ALL=en_US.UTF-8

VOLUME /home/developer/isolation

USER developer
WORKDIR /home/developer
