# JAVA DEVELOPER DOCKERIZED ENVIRONMENT

## Overview
Basically a dockerized java developer environment.
I've been successfully using this for almost one year now and have no regrets whatsoever.
It includes out-of-the-box the most used packages by java developers.
The main dev tool in this version is Eclipse Oxygen. Full stack description can be found bellow:

## Full stack
* Based on Ubuntu 16.04
* Apache Maven 3.5.0
* Eclipse Oxygen JEE
* Eclipse Oxygen RCP
* Oracle JDK 7 Upadte 80
* Oracle JDK 8 Update 141
* Linux packages: `unzip unrar locales tree vim htop git subversion build-essential sudo ssh libgtk-3-0 libxtst-dev`

## Other important info
* The user `developer` (gip:uid = 1000:1000) is the default user. It has sudo (without password) powers.
* The programs are placed into `/opt/`
* There are links in `/usr/bin` folder to the main programs as follows:
    * `mvn`
    * `java`
    * `eclipse-jee`
    * `eclipse-rcp`
* The locale *en_US.UTF-8* is generated and set as default.
* Entrypoint is naturally `/bin/bash` by default.
* There's a helper script called `run.sh` with the parameters necessary to open graphic applications in docker.

## Good practices
### Regular workflow
1. Run a new container with the command `./run --name my_container fabiofranco/java-developer`
2. Enter the container command line by attaching to it with `docker attach my_container`. Do your cli stuff!
3. To run eclipse is best NOT to run it directly from the command line. Instead from your host terminal run `docker exec -it my_container eclipse-jee`. After eclipse starts you can detach from the docker terminal by pressing **Ctrl+P and Ctrl+Q** (dont release the Ctrl key between the P and Q)

### Persistent data
In my experience it is a very effective practice to map a persistent docker volume into an isolated folder that I usually call `isolation` in `$HOME` directory.
This will centralize all the data you need persisted and allow you to consider your container disposable.
Even files or folder you need to be in the home folder AND persisted can be placed there by links. For instance, if I want my maven repository to be persisted (and/or shared) beteween containers I create a maven repository folder into this isolated volume and create a symlink in the home folder as shown in the example bellow.
#### Example: Craeteing a volume and persisting maven repository
```bash
[my_host_user@localhost ~]$ docker volume create test
test
[my_host_user@localhost ~]$ ./run.sh --name my_container -v test:/home/developer/isolation fabiofranco/developer
developer@xxxxxxxxxxxx:~$ sudo chown -R 1000:1000 isolation
developer@xxxxxxxxxxxx:~$ mkdir -p isolation/_env/mvn
developer@xxxxxxxxxxxx:~$ ln -s isolation/_env/mvn .m2
```